from __future__ import annotations
from abc import ABC, abstractmethod


# Netutil
class NetutilAbstractFactory(ABC):
    @abstractmethod
    def create_ping(self) -> PingAbstract:
        pass

    @abstractmethod
    def create_tracert(self) -> TracertAbstract:
        pass


class NetutilWinFactory(NetutilAbstractFactory):
    def create_ping(self) -> PingAbstract:
        return PingWin()

    def create_tracert(self) -> TracertAbstract:
        return TracertWin()


class NetutilMacFactory(NetutilAbstractFactory):
    def create_ping(self) -> PingAbstract:
        return PingMac()

    def create_tracert(self) -> TracertAbstract:
        return TracertMac()


class PingAbstract(ABC):
    @abstractmethod
    def run(self) -> str:
        pass


class PingWin(PingAbstract):
    def run(self) -> str:
        return "Running ping for win..."


class PingMac(PingAbstract):
    def run(self) -> str:
        return "Running ping for mac..."


class TracertAbstract(ABC):
    @abstractmethod
    def run(self) -> None:
        pass


class TracertWin(TracertAbstract):
    def run(self) -> str:
        return "Running traceroute for win..."


class TracertMac(TracertAbstract):
    def run(self) -> str:
        return "Running traceroute for mac..."


class Analyzer(ABC):
    def __init__(self, implementation: NetutilAbstractFactory) -> None:
        self.ping = implementation.create_ping()
        self.tracert = implementation.create_tracert()

    @abstractmethod
    def analyze(self) -> str:
        print(
            f"ping returned: {self.ping.run()}\n"
            f"tracert returned: {self.tracert.run()}\n"
        )


def client_code(activator: Analyzer) -> None:
    activator.activate()


if __name__ == "__main__":
    implementation = NetutilWinFactory()
    analyzer = Analyzer(implementation)
    client_code(analyzer)

    print("\n")

    implementation = NetutilMacFactory()
    analyzer = Analyzer(implementation)
    client_code(analyzer)
